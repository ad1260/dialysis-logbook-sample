import React from "react";
import { NativeBaseProvider, extendTheme} from "native-base";
import { Provider } from "react-redux";
import store from "./store";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AddPotassium from "./screens/adders/AddPotassium";
import AddPhosphate from "./screens/adders/AddPhosphate";
import AddWeight from "./screens/adders/AddWeight";
import AddWellbeing from "./screens/adders/AddWellbeing";
import AddItch from "./screens/adders/AddItch";
import Home from "./screens/Home";
import History from "./screens/History";

//TODO Resolve expo errors so that NavStack can be used instead of a locally declared stack
import NavStack from "./components/NavStack";

// Define the config
const config = {
  useSystemColorMode: false,
  initialColorMode: "light",
};

// extend the theme
export const theme = extendTheme({ config });
type MyThemeType = typeof theme;
declare module "native-base" {
  interface ICustomTheme extends MyThemeType {}
}

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <NativeBaseProvider>
          <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="History" component={History} />
            <Stack.Screen
              name="AddPotassium"
              component={AddPotassium}
              options={{ title: "Add Potassium entry" }}
            />
            <Stack.Screen
              name="AddPhosphate"
              component={AddPhosphate}
              options={{ title: "Add Phosphate entry" }}
            />
            <Stack.Screen
              name="AddWeight"
              component={AddWeight}
              options={{ title: "Add Weight entry" }}
            />
            <Stack.Screen
              name="AddWellbeing"
              component={AddWellbeing}
              options={{ title: "Add Wellbeing entry" }}
            />
            <Stack.Screen
              name="AddItch"
              component={AddItch}
              options={{ title: "Add Itch intensity entry" }}
            />
          </Stack.Navigator>
        </NativeBaseProvider>
      </Provider>
    </NavigationContainer>
  );
}
