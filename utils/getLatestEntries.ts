import { useSelector } from "react-redux";

export default function getLatestEntries() {
  const phosphateLatest = useSelector((state: any) => state.phosphate.latest);
  const potassiumLatest = useSelector((state: any) => state.potassium.latest);
  const weightLatest = useSelector((state: any) => state.weight.latest);
  const wellbeingLatest = useSelector((state: any) => state.wellbeing.latest);
  const itchLatest = useSelector((state: any) => state.itch.latest);

  return [
    phosphateLatest,
    potassiumLatest,
    weightLatest,
    wellbeingLatest,
    itchLatest,
  ];
}
