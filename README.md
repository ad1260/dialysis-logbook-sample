
# Coding assignment for Arman from the best company in Landkreis Miesbach

## Project init

- Create a fork of this repository and clone it

- You can start the app with `yarn start`

- You can either use the iOS Simulator to preview your app or download the Expo Go app for your smartphone and scan the QR-code displayed in your terminal after starting the app

- You are ready to go! Changes are directly visible in Expo Go as fast reload is enabled

### Windows 10 + WSL2
- To start the project first install dependencies:
`yarn install`
`expo doctor --fix-dependencies`
- Then start expo in tunnel mode:
`expo start --tunnel`
- Scan the QR code with the Expo Go app available on both App and Play store.
**Only tested on iPad Air 2 (15.4.1).**

## Misc
To run ESLint:
`"lint": "eslint . --ext .ts"`
ESlint configured with recommended settings for TS.

## Known errors and limitations
1) Magic numbers used in some places for styling. Long term: necessary to extend the default theme, starting with typography settings.
2) Lack of proper form type validation due to a lack of time and knowledge about the topic.
Could potentially use a custom regular expression, or an existing form library to handle inputs better.
3) Several explicit `any` uses. See: #2.
4) Inefficient (but functional) data fetching on `History` screen.
5) Possible children non-unique key warnings. Would be solved by solving #4.
6) Overall insufficient use of polymorphism to better work with an API.
7) `StackNavigator` component could be extracted from `App.tsx`.
8) It was attempted to use a single polymorphic modal for entry input instead of separate Screens, but failed due to #2.
