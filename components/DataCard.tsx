import React from "react";
import { Heading, Text, HStack, Spacer } from "native-base";

interface DataCardProps {
  entry: any;
  hasMetadata: boolean;
}

export default function DataCard({
  entry: entry,
  hasMetadata: hasMetadata,
}: DataCardProps) {
  return (
    <HStack
      w="90%"
      my={1}
      mx={8}
      p={4}
      px={12}
      shadow={4}
      bg="white"
      justifyContent="space-between"
      alignItems="center"
    >
      <Heading pr={2}>{entry.value}</Heading>
      <Spacer />
      {hasMetadata && (
        <>
          <Text>
            {entry.metadataFields[0] == undefined
              ? ""
              : entry.metadataFields[0]}
          </Text>
          <Spacer />
          <Text w="50%" maxW="50%">
            {entry.metadataFields[1] == undefined
              ? ""
              : entry.metadataFields[1]}
          </Text>
        </>
      )}
    </HStack>
  );
}
