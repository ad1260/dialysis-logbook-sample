import React from "react";
import { Box, ScrollView } from "native-base";

export default function Layout({ children }: any) {
  return (
    <>
      <Box
        mx="auto"
        px={8}
        py={4}
        bg="teal.50"
        flex={1}
        flexBasis="0"
        w={{ base: "100%", md: "768px", lg: "1000px", xl: "1080px" }}
      >
        <ScrollView>{children}</ScrollView>
      </Box>
    </>
  );
}
