import React from "react";
import { Heading, Pressable, Text, HStack, VStack } from "native-base";

interface HomeTileProps {
  item: any;
  routeToHistory: () => void;
  routeToAdd: () => void;
  latest: string;
}

export default function HomeTile({
  item,
  routeToHistory,
  routeToAdd,
  latest,
}: HomeTileProps) {
  return (
    <Pressable
      w="100%"
      my={1}
      p={4}
      shadow={4}
      bg="white"
      onPress={() => {
        routeToHistory();
      }}
    >
      <HStack flex={1} alignItems="center" justifyContent="spaceBetween">
        <VStack>
          <Heading size="sm">{item.entryName}</Heading>
          <Text color="muted.500">View History</Text>
        </VStack>
        <VStack alignItems="center" ml="auto">
          <Pressable
            onPress={() => {
              routeToAdd();
            }}
          >
            <VStack alignItems="center" ml="auto">
              {latest == "N / A" ? (
                <Text italic color="muted.300" fontSize={"2xl"}>
                  {latest}
                </Text>
              ) : (
                <Text bold color="muted.600" fontSize={"2xl"}>
                  {latest}
                </Text>
              )}

              <Text color="muted.500">Add new</Text>
            </VStack>
          </Pressable>
        </VStack>
      </HStack>
    </Pressable>
  );
}
