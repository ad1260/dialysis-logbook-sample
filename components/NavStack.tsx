import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AddPotassium from "../screens/adders/AddPotassium";
import AddPhosphate from "../screens/adders/AddPhosphate";
import AddWeight from "../screens/adders/AddWeight";
import AddWellbeing from "../screens/adders/AddWellbeing";
import AddItch from "../screens/adders/AddItch";
import History from "../screens/History";
import Home from "../screens/Home";

const Stack = createNativeStackNavigator();

export default function NavStack() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="History" component={History} />
      <Stack.Screen name="AddPotassium" component={AddPotassium} />
      <Stack.Screen name="AddPhosphate" component={AddPhosphate} />
      <Stack.Screen name="AddWeight" component={AddWeight} />
      <Stack.Screen name="AddWellbeing" component={AddWellbeing} />
      <Stack.Screen name="AddItch" component={AddItch} />
    </Stack.Navigator>
  );
}
