import { createSlice } from "@reduxjs/toolkit";
import {
  Phosphate,
  Potassium,
  Weight,
  Wellbeing,
  Itch,
} from "./types/EntryTypes";

const phosphateTemp: any[] = [];
const phosphateSlice = createSlice({
  name: "phosphate",
  initialState: {
    value: phosphateTemp,
    latest: "N / A",
  },
  reducers: {
    appendPhosphateEntry: (state, action) => {
      state.value.push(action.payload);
      state.latest = action.payload.value;
    },
  },
});

export const { appendPhosphateEntry } = phosphateSlice.actions;
export const phosphateReducer = phosphateSlice.reducer;

const potassiumTemp: any[] = [];
const potassiumSlice = createSlice({
  name: "potassium",
  initialState: {
    value: potassiumTemp,
    latest: "N / A",
  },
  reducers: {
    appendPotassiumEntry: (state, action) => {
      state.value.push(action.payload);
      state.latest = action.payload.value;
    },
  },
});

export const { appendPotassiumEntry } = potassiumSlice.actions;
export const potassiumReducer = potassiumSlice.reducer;

const weightTemp: any[] = [];
const weightSlice = createSlice({
  name: "weight",
  initialState: {
    value: weightTemp,
    latest: "N / A",
  },
  reducers: {
    appendWeightEntry: (state, action) => {
      state.value.push(action.payload);
      state.latest = action.payload.value;
    },
  },
});

export const { appendWeightEntry } = weightSlice.actions;
export const weightReducer = weightSlice.reducer;

const wellbeingTemp: Wellbeing[] = [];
const wellbeingSlice = createSlice({
  name: "wellbeing",
  initialState: {
    value: wellbeingTemp,
    latest: "N / A",
  },
  reducers: {
    appendWellbeingEntry: (state, action) => {
      state.value.push(action.payload);
      state.latest = action.payload.value;
    },
  },
});

export const { appendWellbeingEntry } = wellbeingSlice.actions;
export const wellbeingReducer = wellbeingSlice.reducer;

const itchTemp: Itch[] = [];
const itchSlice = createSlice({
  name: "itch",
  initialState: {
    value: itchTemp,
    latest: "N / A",
  },
  reducers: {
    appendItchEntry: (state, action) => {
      state.value.push(action.payload);
      state.latest = action.payload.value;
    },
  },
});

export const { appendItchEntry } = itchSlice.actions;
export const itchReducer = itchSlice.reducer;
