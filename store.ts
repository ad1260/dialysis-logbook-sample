import { configureStore } from "@reduxjs/toolkit";
import {
  itchReducer,
  phosphateReducer,
  potassiumReducer,
  weightReducer,
  wellbeingReducer,
} from "./logbook-state";

export default configureStore({
  reducer: {
    phosphate: phosphateReducer,
    potassium: potassiumReducer,
    weight: weightReducer,
    wellbeing: wellbeingReducer,
    itch: itchReducer,
  },
});
