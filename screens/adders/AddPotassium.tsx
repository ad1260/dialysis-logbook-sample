import React, { useState } from "react";
import { VStack, FormControl, Input, Text, Button } from "native-base";
import { useDispatch } from "react-redux";
import { appendPotassiumEntry } from "../../logbook-state";
import Layout from "../../components/Layout";
import { Potassium } from "../../types/EntryTypes";

const AddPotassium = ({ navigation }: { navigation: any }) => {
  const dispatch = useDispatch();

  const [measurement, setMeasurement] = useState<string>();
  const [notes, setNotes] = useState<string>();

  return (
    <Layout>
      <VStack mt={16} space={6} alignItems="center">
        <FormControl mt="3" maxWidth="sm">
          <VStack space={6}>
            <FormControl.Label
              accessibilityLabel={"Enter your potassium value"}
              mx="auto"
            >
              Potassium{" "}
              <Text italic color="muted.400">
                mmol
              </Text>
            </FormControl.Label>
            <Input
              type="number"
              value={measurement}
              onChangeText={(value: string) => {
                setMeasurement(value);
              }}
              fontSize="4xl"
              autoFocus={true}
              keyboardType="phone-pad"
              variant="underlined"
            />
            <FormControl.Label
              accessibilityLabel={"Enter your measurement notes"}
              mx="auto"
            >
              Notes
            </FormControl.Label>
            <Input
              type="number"
              value={notes}
              onChangeText={(value: string) => {
                setNotes(value);
              }}
              fontSize="4xl"
              variant="underlined"
            />
          </VStack>
        </FormControl>
        <Button
          w="lg"
          maxW="33%"
          onPress={() => {
            //TODO to use interface Potassium here and prevent invalid text input for value field above
            dispatch(
              appendPotassiumEntry({
                value: measurement,
                metadataFields: [notes],
              })
            );
            navigation.navigate("Home");
          }}
        >
          Enter in log
        </Button>
      </VStack>
    </Layout>
  );
};

export default AddPotassium;
