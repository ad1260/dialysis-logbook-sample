import React, { useState } from "react";
import { VStack, FormControl, Input, Select, Text, Button } from "native-base";
import { useDispatch } from "react-redux";
import { appendWeightEntry } from "../../logbook-state";
import Layout from "../../components/Layout";
import { Weight } from "../../types/EntryTypes";

const AddWeight = ({ navigation }: { navigation: any }) => {
  const dispatch = useDispatch();

  const [measurement, setMeasurement] = useState<string>();
  const [measurementTime, setMeasurementTime] = useState<string>();
  const [notes, setNotes] = useState<string>();

  return (
    <Layout>
      <VStack mt={16} space={6} alignItems="center">
        <FormControl mt="3" maxWidth="sm">
          <VStack space={6}>
            <FormControl.Label
              accessibilityLabel={"Enter your weight"}
              mx="auto"
            >
              Weight{" "}
              <Text italic color="muted.400">
                kg
              </Text>
            </FormControl.Label>
            <Input
              type="number"
              value={measurement}
              onChangeText={(value: string) => {
                setMeasurement(value);
              }}
              fontSize="4xl"
              autoFocus={true}
              keyboardType="phone-pad"
              variant="underlined"
            />
            <FormControl.Label
              accessibilityLabel={"Enter your measurement notes"}
              mx="auto"
            >
              Notes
            </FormControl.Label>
            <Input
              type="text"
              value={notes}
              onChangeText={(value: string) => {
                setNotes(value);
              }}
              fontSize="4xl"
              variant="underlined"
            />
            <FormControl.Label>
              <Select
                minW="100%"
                accessibilityLabel="Select measurement time relative to Dialysis"
                placeholder="Measurement time relative to Dialysis"
                onValueChange={(value) => setMeasurementTime(value)}
              >
                <Select.Item label="Before Dialysis" value="Before Dialysis" />
                <Select.Item label="After Dialysis" value="After Dialysis" />
                <Select.Item label="Arbitrary" value="Arbitrary" />
              </Select>
            </FormControl.Label>
          </VStack>
        </FormControl>
        <Button
          w="lg"
          maxW="33%"
          onPress={() => {
            //TODO to use interface Weight here and prevent invalid text input for value field above
            dispatch(
              appendWeightEntry({
                value: measurement,
                metadataFields: [measurementTime, notes],
              })
            );
            navigation.navigate("Home");
          }}
        >
          Enter in log
        </Button>
      </VStack>
    </Layout>
  );
};

export default AddWeight;
