import React, { useState } from "react";
import { VStack, Slider, Text, Heading, Button } from "native-base";
import { useDispatch } from "react-redux";
import { appendItchEntry } from "../../logbook-state";
import Layout from "../../components/Layout";
import { Itch } from "../../types/EntryTypes";

const AddItch = ({ navigation }: { navigation: any }) => {
  const dispatch = useDispatch();

  const [measurement, setMeasurement] = useState<number>(5); // Must be equal to Slider's defaultValue prop!

  return (
    <Layout>
      <VStack mt={16} space={6} alignItems="center">
        <Text color="muted.600">Describe your itch intensity:</Text>
        <Heading>{measurement}</Heading>
        <Slider
          w="3/4"
          maxW="300"
          defaultValue={5}
          minValue={1}
          maxValue={10}
          accessibilityLabel="Itch intensity"
          step={1}
          onChange={(v) => {
            setMeasurement(Math.floor(v));
          }}
        >
          <Slider.Track>
            <Slider.FilledTrack />
          </Slider.Track>
          <Slider.Thumb />
        </Slider>
        <Button
          w="lg"
          maxW="33%"
          onPress={() => {
            const submitValue: Itch = { value: measurement };
            dispatch(appendItchEntry(submitValue));
            navigation.navigate("Home");
          }}
        >
          Enter in log
        </Button>
      </VStack>
    </Layout>
  );
};

export default AddItch;
