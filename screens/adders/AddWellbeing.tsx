import React, { useState } from "react";
import { Slider, Text, VStack, Heading, Button } from "native-base";
import { useDispatch } from "react-redux";
import { appendWellbeingEntry } from "../../logbook-state";
import Layout from "../../components/Layout";
import { Wellbeing } from "../../types/EntryTypes";

const AddWellbeing = ({ navigation }: { navigation: any }) => {
  const dispatch = useDispatch();

  const [measurement, setMeasurement] = useState<number>(4); // Must be equal to Slider's defaultValue prop!

  return (
    <Layout>
      <VStack mt={16} space={6} alignItems="center">
        <Text color="muted.600">Describe your wellbeing:</Text>
        <Heading>{measurement}</Heading>
        <Slider
          w="3/4"
          maxW="300"
          defaultValue={4}
          minValue={1}
          maxValue={6}
          accessibilityLabel="Wellbeing"
          step={1}
          onChange={(v) => {
            setMeasurement(Math.floor(v));
          }}
        >
          <Slider.Track>
            <Slider.FilledTrack />
          </Slider.Track>
          <Slider.Thumb />
        </Slider>
        <Button
          w="lg"
          maxW="33%"
          onPress={() => {
            const submitValue: Wellbeing = { value: measurement };
            dispatch(appendWellbeingEntry(submitValue));
            navigation.navigate("Home");
          }}
        >
          Enter in log
        </Button>
      </VStack>
    </Layout>
  );
};

export default AddWellbeing;
