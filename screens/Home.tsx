import React from "react";
import { HStack, VStack, Heading, Text } from "native-base";
import Layout from "../components/Layout";
import HomeTile from "../components/HomeTile";
import getLatestEntries from "../utils/getLatestEntries";

export default function Home({ navigation }: { navigation: any }) {
  const latestEntriesArray = getLatestEntries(); //phosphate, potassium, weight, wellbeing, itch

  function navigateToHistory(entryType: string, entryAddRoute: string) {
    navigation.navigate("History", {
      entryType: entryType,
      entryAddRoute: entryAddRoute,
    });
  }

  return (
    <Layout>
      <VStack flex={1}>
        <HStack
          my={6}
          mx={2}
          alignItems="baseline"
          justifyContent="space-between"
        >
          <Heading color="muted.700"> My values </Heading>
          <Text color="muted.600">Latest entries:</Text>
        </HStack>

        {tileData.map((x, index) => {
          return (
            <HStack key={index}>
              <HomeTile
                item={x}
                routeToHistory={() =>
                  navigateToHistory(x.entryType, x.entryAddRoute)
                }
                routeToAdd={() => navigation.navigate(x.entryAddRoute)}
                latest={latestEntriesArray[index]}
              ></HomeTile>
            </HStack>
          );
        })}
      </VStack>
    </Layout>
  );
}

const tileData = [
  {
    entryType: "PHOSPHATE",
    entryName: "Phosphate",
    entryAddRoute: "AddPhosphate",
  },
  {
    entryType: "POTASSIUM",
    entryName: "Potassium",
    entryAddRoute: "AddPotassium",
  },
  {
    entryType: "WEIGHT",
    entryName: "Weight",
    entryAddRoute: "AddWeight",
  },
  {
    entryType: "WELLBEING",
    entryName: "Wellbeing",
    entryAddRoute: "AddWellbeing",
  },
  {
    entryType: "ITCH_INTENSITY",
    entryName: "Itch intensity",
    entryAddRoute: "AddItch",
  },
];
