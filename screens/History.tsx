import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Stack, Text, VStack, Button, Heading } from "native-base";
import Layout from "../components/Layout";
import DataCard from "../components/DataCard";

export default function History({ route, navigation }: any) {
  const wellbeingState = useSelector((state: any) => state.wellbeing.value);
  const itchState = useSelector((state: any) => state.itch.value);
  const weightState = useSelector((state: any) => state.weight.value);
  const phosphateState = useSelector((state: any) => state.phosphate.value);
  const potassiumState = useSelector((state: any) => state.potassium.value);

  const [phosphateItems, setPhosphateItems] = useState<any[]>([]);
  const [potassiumItems, setPotassiumItems] = useState<any[]>([]);
  const [weightItems, setWeightItems] = useState<any[]>([]);
  const [wellbeingItems, setWellbeingItems] = useState<any[]>([]);
  const [itchItems, setItchItems] = useState<any[]>([]);

  const { entryType, entryAddRoute } = route.params;

  //TODO generate only one array instead of all five
  //TODO 'fix non-unique key for children' warning
  useEffect(() => {
    let temp: any[] = [];
    phosphateState.forEach((entry: any, index: any) => {
      temp.push(
        <Stack key={index}>
          <DataCard entry={entry} hasMetadata={true} />
        </Stack>
      );
    });
    setPhosphateItems(temp.reverse());

    temp = [];
    potassiumState.forEach((entry: any, index: any) => {
      temp.push(
        <Stack key={index}>
          <DataCard entry={entry} hasMetadata={true} />
        </Stack>
      );
    });
    setPotassiumItems(temp.reverse());

    temp = [];
    weightState.forEach((entry: any, index: any) => {
      temp.push(
        <Stack key={index}>
          <DataCard entry={entry} hasMetadata={true} />
        </Stack>
      );
    });
    setWeightItems(temp.reverse());

    temp = [];
    wellbeingState.forEach((entry: any, index: any) => {
      temp.push(
        <Stack key={index}>
          <DataCard entry={entry} hasMetadata={false} />
        </Stack>
      );
    });
    setWellbeingItems(temp.reverse());

    temp = [];
    itchState.forEach((entry: any, index: any) => {
      temp.push(
        <Stack key={index}>
          <DataCard entry={entry} hasMetadata={false} />
        </Stack>
      );
    });
    setItchItems(temp.reverse());
  }, []);

  function itemSwitch() {
    switch (entryType) {
      case "PHOSPHATE": {
        return phosphateItems;
      }
      case "POTASSIUM": {
        return potassiumItems;
      }
      case "WEIGHT": {
        return weightItems;
      }
      case "WELLBEING": {
        return wellbeingItems;
      }
      case "ITCH_INTENSITY": {
        return itchItems;
      }
    }
  }

  return (
    <Layout>
      <VStack w="100%" alignItems="center">
        <Heading mt={8} color="muted.500" fontSize={"lg"}>
          HISTORY OF {entryType} ENTRIES
        </Heading>
        <Button
          my={8}
          w="25%"
          rounded={"2xl"}
          onPress={() => navigation.navigate(entryAddRoute)}
        >
          Add New
        </Button>
        {itemSwitch()}
      </VStack>
    </Layout>
  );
}
