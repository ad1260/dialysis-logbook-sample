export interface Phosphate  {
  value: number;
  metadataFields: string [];
}

export interface Potassium  {
  value: number;
  metadataFields: string[];
}

export interface Weight  {
  value: number;
  metadataFields: string[];
}

export interface Wellbeing  {
  value: number;
}

export interface Itch  {
  value: number;
}